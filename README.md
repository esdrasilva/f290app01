# F290App01

## Instruções

Neste app, reutilizei o exemplo utilizado em sala para poder concluí-lo adicionando
algumas propriedades que não foram abordadas em sala devido à condição atual das máquinas,
mas não se preocupem pois comentei todo código nos trechos que considerei de importância
em percurso inicial de aprendizagem, não deixem de visualizar os comentários.

### Arquivos comentados
Arquivo | Funcionalidade
-- | --
activity_main.xml  |  Arquivo de UI. Tudo que vemos e tocamos está aqui. Seria uma espécie de **front-end**.
MainActivity.java  |  Arquivo de codificação em Java. Responsável pelo gerenciamento de interações e transformações de informações, as regras de negócio ficam aqui. Seria nosso **back-end**.
strings.xml  |  Arquivo contendo os recursos textuais que venham a ser utilizado em nossa aplicação.
colors.xml  |  Arquivo contendo as cores reutilizadas na aplicação.
style.xml  |  Assim como no CSS, podemos criar nossos estilos neste arquivo.


Nos arquivos acima citados, contém comentários para facilitar o entendimento de cada um deles.

Aproveitem!
