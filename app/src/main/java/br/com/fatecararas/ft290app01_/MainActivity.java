package br.com.fatecararas.ft290app01_;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    /**
     * Declaracao objetos para interagir com os widgets(objetos visuais)
     Declaramos fora do metodo onCreate para poder acessa-los globalmente no metodo calcular()
     */
    EditText editTextValor1, editTextValor2;
    TextView textViewResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Neste ponto estamos fazendo a ligacao entre os elementos visuais do .xml, com os objetos
         * Java. Para tal utilizamos o metodo findViewById('id_definido_por_voce').
         * Após este trecho, podemos acessar os objetos da UI e os manupular via programação.
         */
        editTextValor1 = findViewById(R.id.editTextValor1);
        editTextValor2 = findViewById(R.id.editTextValor2);
        textViewResultado = findViewById(R.id.textViewResultado);
    }

    /**
     * Metodo responsavel por realizar o calculo em nossa simples calculadora
     * @param view recebe como argumento, a referencia do Widget que realizou a chamada de função,
     *             com ele podemos identificar o objeto e realizar a ação necessária.
     *             Vamos capturar o texto deste objeto e exibi-lo no Log, verifique a linha 46.
     */
    public void calcular(View view){
        Float resultado = 0F;

        if(isEntradaValida()){

            Float valor1 = Float.parseFloat(editTextValor1.getText().toString());
            Float valor2 = Float.parseFloat(editTextValor2.getText().toString());
            String textoBotao = ((Button) view).getText().toString();
            Log.d(TAG,": Operacao - "+textoBotao);

            /**
             * No switch(), capturamos a o valor do ID pelo metodo acessor getId() atraves do objeto view
             */
            switch (view.getId()){
                case R.id.buttonSomar:
                    /**  Neste trecho iremos exibir no Logcat, informações sobre o debug.
                     *  Caso o id recebido seja igual ao identificador R.id.id_definido_por_voce,
                     *    o Log sera exibido
                     */
                    Log.d(TAG, " - Click: SOMA");
                    resultado = valor1 + valor2;
                    break;
                case R.id.buttonSubtrair:
                    Log.d(TAG, " - Click: SUBTRACAO");
                    resultado = valor1 - valor2;
                    break;
                case R.id.buttonMultiplicar:
                    Log.d(TAG, " - Click: MULTIPLICACAO");
                    resultado = valor1 * valor2;
                    break;
                case R.id.buttonDividir:
                    Log.d(TAG, " - Click: DIVISAO");
                    resultado = valor1 / valor2;
                    break;
                default:
            }
            textViewResultado.setText(String.format("%.2f",resultado));

        }else {
            Toast.makeText(getApplicationContext(), "Não esqueça de preencher os dois valores!",
                    Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Funcao para validar se as duas entradas de dados foram preenchidas, para nao ocorrerem excessoes
     *   de parsing de Float e erros nos calculos.
     * @return boolean
     */
    public boolean isEntradaValida(){
        if (TextUtils.isEmpty(editTextValor1.getText().toString())
                || TextUtils.isEmpty(editTextValor2.getText().toString())) {
            return false;
        }else{
            return true;
        }
    }
}
